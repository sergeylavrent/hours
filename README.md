# hours
A simple program calculating times of the day given durations.

Nothing special, really, but something I needed myself and one of my earliest projects.

```sh
$ ./hours 12.34 + 5.00
17.34

$ ./hours 12.34 - 0.34
12.00

$ ./hours 23 + 6
5.00
```

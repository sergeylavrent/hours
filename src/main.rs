use std::{env::args, process};

enum Keyword { Plus, Minus }

const PLUS_KEYWORDS_LIST: [&str; 7] = [
    "+",
    "add",
    "Add",
    "ADD",
    "plus",
    "Plus",
    "PLUS"
];

const MINUS_KEYWORDS_LIST: [&str; 13] = [
    "-",
    "rem",
    "Rem",
    "REM",
    "remove",
    "Remove",
    "REMOVE",
    "minus",
    "Minus",
    "MINUS",
    "take",
    "Take",
    "TAKE"
];

fn main() {
    let args: Vec<String> = args().collect();

    if args.len() < 4 {
        eprintln!("
Not enough arguments are given!
    
Format:\t\tTIME + KEYWORD + TIME

Time form:\t\t<hours (24h-format)>.<minutes>
Keyword format:\t\t+, -, plus, minus, add, rem/remove
        ");
        process::exit(1);
    }

    println!("{:.2}", get_result(
        get_hour(&args[1]),
        get_keyword(&args[2]),
        get_duration(&args[3])
    ));
}

fn get_hour(hour: &str) -> f32 {
    match hour.parse::<f32>() {
        Ok(hour) => {
            if !(0..24).contains(&(hour.floor() as u8))
            || !(0..60).contains(&(hour.fract() as u8 * 100)) {
                eprintln!("The given time is not valid!");
                process::exit(1);
            }

            return hour;
        },
        Err(_) => {
            eprintln!("The input is not a number!");
            process::exit(1);
        }
    }
}

fn get_keyword(keyword: &str) -> Keyword {
    if PLUS_KEYWORDS_LIST.contains(&keyword) { return  Keyword::Plus; }
    if MINUS_KEYWORDS_LIST.contains(&keyword) { return Keyword::Minus }

    eprintln!("The second argument is invalid!");
    process::exit(1);
}

fn get_duration(hour: &str) -> f32 {
    match hour.parse::<f32>() {
        Ok(hour) => {
            if hour < 0. || !(0..60).contains(&(hour.fract() as u8 * 100)) {
                eprintln!("The given time is not valid!");
                process::exit(1);
            }

            return hour;
        },
        Err(_) => {
            eprintln!("The input is not a number!");
            process::exit(1);
        }
    }
}

fn get_result(hour: f32, keyword: Keyword, duration: f32) -> f32 {
    let mut hours = hour.floor() + match keyword {
        Keyword::Plus => duration.floor(),
        Keyword::Minus => 24. - duration.floor()
    };

    let mut minutes = hour.fract() + match keyword {
        Keyword::Plus => duration.fract(),
        Keyword::Minus => 0.6 - duration.fract()
    };

    while minutes > 0.59 {
        minutes -= 0.6;
        hours += 1.;
    }

    (hours % 24.) + minutes
}
